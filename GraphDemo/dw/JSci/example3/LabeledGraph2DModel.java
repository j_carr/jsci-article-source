package dw.JSci.example3;

/**
 * 2D Graph Model containing Labels
 * @author: John Carr
 */
 
public interface LabeledGraph2DModel 
		extends JSci.awt.Graph2DModel {
			
/**
 * Get the String value for
 * the X coord
 */
String getXLabel(float i);
}
