package dw.JSci.example3;

/**
 * The model used by the LineGraph 
 * example
 * @author: John Carr
 */
import java.util.*;
 
public class DemandGraphModel 
	extends JSci.awt.AbstractGraphModel 
		implements LabeledGraph2DModel {
	
	private Vector allSeries_ = new Vector();
	private Enumeration seriesEnumeration;
	private int[] xAxis;
	private String[] xAxisLabel;
	private float[] currentSeries;

public DemandGraphModel() { super(); }
public void addSeries(float[] series) {
	
	allSeries_.addElement(series);
	
}
public void firstSeries() {
	
	seriesEnumeration = allSeries_.elements();

	if (seriesEnumeration.hasMoreElements())
		currentSeries = (float[]) seriesEnumeration.nextElement();
	
}
public float getSeriesCellValue(int seriesNumber, int position) {

	float[] series 
		= (float[])allSeries_.elementAt(seriesNumber);

	return series[position];
	
}
public float getXCoord(int i) {

	return xAxis[i];
	
}
public String getXLabel(float i) {

	return xAxisLabel[(int)i];
	
}
public float getYCoord(int i) {
	
	return currentSeries[i];
	
}
public boolean nextSeries() {
	
	boolean newSeries = false;
	
	if (seriesEnumeration.hasMoreElements())
	{
		newSeries = true; 
		currentSeries 
			= (float[]) seriesEnumeration.nextElement();
	}
		
	return newSeries;
	
}
public int seriesLength() {
	
	return currentSeries.length;
	
}
public void setXAxis(int[] x) {
	
	xAxis = x;
	
}
public void setXAxisLabel(String[] x) {
	
	xAxisLabel = x;
	
}
}
