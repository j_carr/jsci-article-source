package dw.JSci.example3;

/**
 * Line Graph Example
 * @author: John Carr
 */
import java.awt.*;
import java.awt.event.*;
import JSci.swing.*;
import javax.swing.*;

public class LineGraph extends JFrame {

	private JLineGraph lineGraph;
	private static final String MONTH_NAMES[] = { "Jan", "Feb", "Mar", "Apr", "May", "Jun" };
	private static final int MONTH_NUMBERING[] = {0, 1, 2, 3, 4, 5 }; 
	private static final int LONDON_SERIES 	= 0;
	private static final int PARIS_SERIES	= 1;
	private static final int NEW_YORK_SERIES= 2;
	private static final float LONDON_DEMAND[] = { 1.2f, 3.7f, 6.7f, 4.6f, 6.4f, 3.8f };
	private static final float PARIS_DEMAND[] = { 12.6f, 15.0f, 13.7f, 15.8f, 10.0f, 11.9f };
	private static final float NEW_YORK_DEMAND[] = { 15.0f, 13.9f, 10.1f, 12.4f, 6.1f, 8.0f };		
	private JPanel graphPane;
	private JPanel legendPanel;	
	private JLabel londonLabel;	
	private JLabel newYorkLabel;	
	private JLabel parisLabel;	
	private JLabel titleLabel;

public LineGraph() {
	super("Line Graph Example");
	initialize();
}
private JPanel getGraphPane() {
	if (graphPane == null) {
		try {
			graphPane = new JPanel();
			graphPane.setLayout(new java.awt.BorderLayout());
			getGraphPane().add(getTitleLabel(), "North");
			getGraphPane().add(getLegendPanel(), "East");
			getGraphPane().add(getLineGraph(), "Center");
		} catch (java.lang.Throwable t) {
			handleException(t);
		}
	}
	return graphPane;
}
private JPanel getLegendPanel() {
	if (legendPanel == null) {
		try {
			legendPanel = new JPanel();
			legendPanel.setLayout(new GridBagLayout());

			GridBagConstraints cParisLabel 
				= new GridBagConstraints();
			cParisLabel.gridx = 0; 
			cParisLabel.gridy = 1;
			cParisLabel.anchor 
				= GridBagConstraints.WEST;
			cParisLabel.insets 
				= new Insets(4, 4, 4, 4);
			getLegendPanel().add(getParisLabel(), 
								 cParisLabel);

			GridBagConstraints cLondonLabel 
				= new GridBagConstraints();
			cLondonLabel.gridx = 0; 
			cLondonLabel.gridy = 0;
			cLondonLabel.anchor 
				= GridBagConstraints.WEST;
			cLondonLabel.insets 
				= new Insets(4, 4, 4, 4);
			getLegendPanel().add(getLondonLabel(), 
								 cLondonLabel);

			GridBagConstraints cNewYorkLabel 
				= new GridBagConstraints();
			cNewYorkLabel.gridx = 0; 
			cNewYorkLabel.gridy = 2;
			cNewYorkLabel.anchor 
				= GridBagConstraints.WEST;
			cNewYorkLabel.insets 
				= new Insets(4, 4, 4, 4);
			getLegendPanel().add(getNewYorkLabel(), 
								 cNewYorkLabel);
		} catch (Throwable t) {
			handleException(t);
		}
	}
	return legendPanel;
}
private JLineGraph getLineGraph() {
	DemandGraphModel model = new DemandGraphModel();
	model.addSeries(LONDON_DEMAND);
	model.addSeries(PARIS_DEMAND);
	model.addSeries(NEW_YORK_DEMAND);
	model.setXAxisLabel(MONTH_NAMES);
	model.setXAxis(MONTH_NUMBERING);
	
	lineGraph = new LabeledLineGraph(model);
	lineGraph.setColor(LONDON_SERIES, Color.red);
	lineGraph.setColor(PARIS_SERIES, Color.yellow);
	lineGraph.setColor(NEW_YORK_SERIES, Color.blue);
	lineGraph.setXIncrement(1);
	
	return lineGraph;
}
private JLabel getLondonLabel() {
	if (londonLabel == null) {
		try {
			londonLabel = new JLabel();
			londonLabel.setText("London");
			londonLabel.setForeground(Color.red);
		} catch (Throwable t) {
			handleException(t);
		}
	}
	return londonLabel;
}
private JLabel getNewYorkLabel() {
	if (newYorkLabel == null) {
		try {
			newYorkLabel = new JLabel();
			newYorkLabel.setText("New York");
			newYorkLabel.setForeground(Color.blue);
		} catch (Throwable t) {
			handleException(t);
		}
	}
	return newYorkLabel;
}
private JLabel getParisLabel() {
	if (parisLabel == null) {
		try {
			parisLabel = new JLabel();
			parisLabel.setText("Paris");
			parisLabel.setForeground(Color.yellow);
		} catch (Throwable t) {
			handleException(t);
		}
	}
	return parisLabel;
}
private JLabel getTitleLabel() {
	if (titleLabel == null) {
		try {
			titleLabel = new JLabel();
			titleLabel.setText("Coffee Bean Demand for 1st and 2nd Quarter (In Tons)");
		} catch (Throwable t) {
			handleException(t);
		}
	}
	return titleLabel;
}
/**
 * Called whenever the part throws an exception.
 * @param exception java.lang.Throwable
 */
private void handleException(Throwable t) {

	t.printStackTrace(System.out);
}
private void initialize() {
	try {
		setName("LineGraph");
		setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
		setSize(600, 480);
		setContentPane(getGraphPane());
	} catch (Throwable t) {
		handleException(t);
	}
}
}
