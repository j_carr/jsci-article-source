package dw.JSci.example1;

/**
 * Pie Graph Example
 * @author: John Carr
 */
import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import JSci.awt.*;
import JSci.swing.*;
 
public class PieGraph extends JFrame 
			implements ItemListener {
	private JPanel graphPane;
	private JComboBox londonComboBox;
	private JLabel londonLabel;
	private JComboBox newYorkComboBox;
	private JLabel newYorkLabel;
	private JPanel optionPanel;
	private JComboBox parisComboBox;
	private JLabel parisLabel;
	private JLabel titleLabel;
	private JPieChart pieChart;
	private static final String CATEGORY_NAMES[] 
			= { "London", "Paris", "New York" };
	private static final int LONDON 	
			= 0;
	private static final int PARIS
			= 1;
	private static final int NEW_YORK
			= 2;
	private static final float CONSUMERS_SERIES[] 
			= { 45.3f, 27.1f, 55.5f };
	
	private static final String BLUE 	
			= "Blue";
	private static final String RED 	
			= "Red";
	private static final String GREEN	
			= "Green";
	private static final String CYAN
			= "Cyan";
	private static final String MAGENTA
			= "Magenta";
	private static final String ORANGE
			= "Orange";
	private static final String PINK
			= "Pink";
	private static final String YELLOW
			= "Yellow";
	private static final String WHITE
			= "White";

public PieGraph() {
	super("Pie Chart Example");
	initialize();
}
/**
 * Change the category color 
 */
private void changeColor(int city, 	
						 String colorName) {

	Color selectedColor = Color.black;
	
	if (colorName.equals(RED)) 
		selectedColor = Color.red;

	else if (colorName.equals(BLUE))
		selectedColor = Color.blue;

	else if (colorName.equals(GREEN))
		selectedColor = Color.green;

	else if (colorName.equals(CYAN)) 
		selectedColor = Color.cyan;

	else if (colorName.equals(MAGENTA))
		selectedColor = Color.magenta;

	else if (colorName.equals(ORANGE))
		selectedColor = Color.orange;

	else if (colorName.equals(PINK)) 
		selectedColor = Color.pink;

	else if (colorName.equals(YELLOW))
		selectedColor = Color.yellow;

	else if (colorName.equals(WHITE))
		selectedColor = Color.white;
	
	pieChart.setColor(city, 
					  selectedColor);

	pieChart.redraw();
	}
private JPanel getGraphPane() {
	if (graphPane == null) {
		try {
			graphPane = new JPanel();
			graphPane.setLayout(new BorderLayout());
			graphPane.add(getOptionPanel(), 
						  "East");
			graphPane.add(getTitleLabel(), 
						  "North");
			graphPane.add(getPieGraph(), 
				          "Center");
		} catch (Throwable t) {
			handleException(t);
		}
	}
	return graphPane;
}
private JComboBox getLondonComboBox() {
	if (londonComboBox == null) {
		try {
			londonComboBox = new JComboBox();
			londonComboBox.setMaximumRowCount(3);
			londonComboBox.addItem(RED);
			londonComboBox.addItem(BLUE);
			londonComboBox.addItem(GREEN);
			londonComboBox.setSelectedItem(BLUE);
			londonComboBox.addItemListener(this);
		} catch (Throwable t) {
			handleException(t);
		}
	}
	return londonComboBox;
}
private JLabel getLondonLabel() {
	if (londonLabel == null) {
		try {
			londonLabel = new JLabel();
			londonLabel.setText("London");
		} catch (Throwable t) {
			handleException(t);
		}
	}
	return londonLabel;
}
private JComboBox getNewYorkComboBox() {
	if (newYorkComboBox == null) {
		try {
			newYorkComboBox = new JComboBox();
			newYorkComboBox.setMaximumRowCount(3);
			newYorkComboBox.addItem(CYAN);
			newYorkComboBox.addItem(MAGENTA);
			newYorkComboBox.addItem(ORANGE);
			newYorkComboBox.setSelectedItem(ORANGE);
			newYorkComboBox.addItemListener(this);
		} catch (Throwable t) {
			handleException(t);
		}
	}
	return newYorkComboBox;
}
private JLabel getNewYorkLabel() {
	if (newYorkLabel == null) {
		try {
			newYorkLabel = new JLabel();
			newYorkLabel.setText("New York");
		} catch (Throwable t) {
			handleException(t);
		}
	}
	return newYorkLabel;
}
private JPanel getOptionPanel() {
	if (optionPanel == null) {
		try {
			optionPanel = new JPanel();
			optionPanel.setLayout(new GridBagLayout());

			GridBagConstraints cLondonLabel 
					= new GridBagConstraints();
			cLondonLabel.gridx = 1; 
			cLondonLabel.gridy = 1;
			cLondonLabel.insets 
					= new Insets(4, 4, 4, 4);
			optionPanel.add(getLondonLabel(), 
				            cLondonLabel);

			GridBagConstraints cLondonComboBox 
					= new GridBagConstraints();
			cLondonComboBox.gridx = 2; 
			cLondonComboBox.gridy = 1;
			cLondonComboBox.fill 
					= GridBagConstraints.HORIZONTAL;
			cLondonComboBox.weightx = 1.0;
			cLondonComboBox.insets 
					= new Insets(4, 4, 4, 4);
			optionPanel.add(getLondonComboBox(), 
							cLondonComboBox);

			GridBagConstraints cParisLabel 
					= new GridBagConstraints();
			cParisLabel.gridx = 1; 
			cParisLabel.gridy = 2;
			cParisLabel.insets 
					= new Insets(4, 4, 4, 4);
			optionPanel.add(getParisLabel(), 
							cParisLabel);

			GridBagConstraints cParisComboBox 
					= new GridBagConstraints();
			cParisComboBox.gridx = 2; 
			cParisComboBox.gridy = 2;
			cParisComboBox.fill 
					= GridBagConstraints.HORIZONTAL;
			cParisComboBox.weightx = 1.0;
			cParisComboBox.insets 
					= new Insets(4, 4, 4, 4);
			optionPanel.add(getParisComboBox(), 
							cParisComboBox);

			GridBagConstraints cNewYorkLabel 
					= new GridBagConstraints();
			cNewYorkLabel.gridx = 1; 
			cNewYorkLabel.gridy = 3;
			cNewYorkLabel.insets 
					= new Insets(4, 4, 4, 4);
			optionPanel.add(getNewYorkLabel(), 
							cNewYorkLabel);

			GridBagConstraints cNewYorkComboBox 
					= new GridBagConstraints();
			cNewYorkComboBox.gridx = 2; 
			cNewYorkComboBox.gridy = 3;
			cNewYorkComboBox.fill 
					= GridBagConstraints.HORIZONTAL;
			cNewYorkComboBox.weightx = 1.0;
			cNewYorkComboBox.insets 
					= new Insets(4, 4, 4, 4);
			optionPanel.add(getNewYorkComboBox(), 
							cNewYorkComboBox);
		} catch (Throwable t) {
			handleException(t);
		}
	}
	return optionPanel;
}
private JComboBox getParisComboBox() {
	if (parisComboBox == null) {
		try {
			parisComboBox = new JComboBox();
			parisComboBox.setMaximumRowCount(3);
			parisComboBox.addItem(PINK);
			parisComboBox.addItem(YELLOW);
			parisComboBox.addItem(WHITE);
			parisComboBox.setSelectedItem(YELLOW);
			parisComboBox.addItemListener(this);
		} catch (Throwable t) {
			handleException(t);
		}
	}
	return parisComboBox;
}
private JLabel getParisLabel() {
	if (parisLabel == null) {
		try {
			parisLabel = new JLabel();
			parisLabel.setText("Paris");
		} catch (Throwable t) {
			handleException(t);
		}
	}
	return parisLabel;
}
private JPieChart getPieGraph() {
	DefaultCategoryGraph2DModel model
		=new DefaultCategoryGraph2DModel();
	model.setCategories(CATEGORY_NAMES);
	model.addSeries(CONSUMERS_SERIES);
	pieChart = new JPieChart(model);
	pieChart.setColor(LONDON,Color.blue);
	pieChart.setColor(PARIS,Color.yellow);
	pieChart.setColor(NEW_YORK,Color.orange);

	return pieChart;
}
private JLabel getTitleLabel() {
	if (titleLabel == null) {
		try {
			titleLabel = new JLabel();
			titleLabel.setText("Percentage of Coffee Drinkers");
		} catch (Throwable t) {
			handleException(t);
		}
	}
	return titleLabel;
}
private void handleException(Throwable exception) {

	exception.printStackTrace(System.out);
}
private void initialize() {
	try {
		setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
		setSize(600, 480);
		setContentPane(getGraphPane());
	} catch (Throwable t) {
		handleException(t);
	}
}
public void itemStateChanged(ItemEvent e) 
{
	if(e.getSource() == getLondonComboBox()
		&& e.getStateChange() == e.SELECTED) {

		changeColor(LONDON,
			(String)getLondonComboBox().getSelectedItem());
	}

	else if(e.getSource() == getParisComboBox()
		&& e.getStateChange() == e.SELECTED) {

		changeColor(PARIS,
			(String)getParisComboBox().getSelectedItem());
	}
		
	else if(e.getSource() == getNewYorkComboBox()
		&& e.getStateChange() == e.SELECTED) {

		changeColor(NEW_YORK,
			(String)getNewYorkComboBox().getSelectedItem());
	}
}
}
