package dw.JSci.example2;

/**
 * Bar Graph Example 
 * @author: John Carr
 */
import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import JSci.swing.*;
 
public class BarGraph extends JFrame 
			implements ActionListener {

	private JButton addButton;
	private JRadioButton londonRadioButton;
	private JRadioButton newYorkRadioButton;
	private JRadioButton parisRadioButton;
	private JPanel graphPane;
	private JPanel optionPanel;
	private JLabel titleLabel;
	private JBarGraph barGraph;
	private static final String CATEGORY_NAMES[] 
			= { "London", "Paris", "New York" };
	private static final int LONDON 	
			= 0;
	private static final int PARIS
			= 1;
	private static final int NEW_YORK
			= 2;
	private static final float SALES_SERIES[] 
			= { 24.1f, 14.4f, 36.8f };

public BarGraph() {
	super("Bar Graph Example");
	initialize();
}
public void actionPerformed(ActionEvent e) {
	if (e.getSource() == getAddButton()) {

		if (getLondonRadioButton().isSelected()) 
			addToCategoryTotal(LONDON);
		else if (getNewYorkRadioButton().isSelected()) 
			addToCategoryTotal(NEW_YORK);
		else if (getParisRadioButton().isSelected()) 
			addToCategoryTotal(PARIS);
 
		barGraph.dataChanged(null);
	}
}
private void addToCategoryTotal(int category) {
	SalesGraphModel model 
			= (SalesGraphModel)barGraph.getModel();

	model.incrementCategoryTotal(category);
}
private JButton getAddButton() {
	if (addButton == null) {
		try {
			addButton = new JButton();
			addButton.setText("Add 1 to Total");
			addButton.addActionListener(this);
		} catch (Throwable t) {
			handleException(t);
		}
	}
	return addButton;
}
private JBarGraph getBarGraph() {
	SalesGraphModel model 
		= new SalesGraphModel(CATEGORY_NAMES, 
							  SALES_SERIES);
	barGraph = new JBarGraph(model);
	return barGraph;
}
private JPanel getGraphPane() {
	if (graphPane == null) {
		try {
			graphPane = new JPanel();
			graphPane.setLayout(new BorderLayout());
			graphPane.add(getOptionPanel(), "East");
			graphPane.add(getTitleLabel(), "North");
			graphPane.add(getBarGraph(), "Center");
		} catch (Throwable t) {
			handleException(t);
		}
	}
	return graphPane;
}
private JRadioButton getLondonRadioButton() {
	if (londonRadioButton == null) {
		try {
			londonRadioButton = new JRadioButton();
			londonRadioButton.setSelected(true);
			londonRadioButton.setText("London");
		} catch (Throwable t) {
			handleException(t);
		}
	}
	return londonRadioButton;
}
private JRadioButton getNewYorkRadioButton() {
	if (newYorkRadioButton == null) {
		try {
			newYorkRadioButton = new JRadioButton();
			newYorkRadioButton.setText("New York");
		} catch (Throwable t) {
			handleException(t);
		}
	}
	return newYorkRadioButton;
}
private JPanel getOptionPanel() {
	if (optionPanel == null) {
		try {
			optionPanel = new JPanel();
			optionPanel.setLayout(new GridBagLayout());

			GridBagConstraints cLondonRadioButton 
				= new GridBagConstraints();
			cLondonRadioButton.gridx = 0; 
			cLondonRadioButton.gridy = 0;
			cLondonRadioButton.anchor 
				= GridBagConstraints.WEST;
			cLondonRadioButton.insets 
				= new Insets(4, 4, 4, 4);
			optionPanel.add(getLondonRadioButton(), 
							cLondonRadioButton);

			GridBagConstraints cParisRadioButton 
				= new GridBagConstraints();
			cParisRadioButton.gridx = 0; 
			cParisRadioButton.gridy = 1;
			cParisRadioButton.anchor 
				= GridBagConstraints.WEST;
			cParisRadioButton.insets 
				= new Insets(4, 4, 4, 4);
			optionPanel.add(getParisRadioButton(), 
							cParisRadioButton);

			GridBagConstraints cNewYorkRadioButton 
				= new GridBagConstraints();
			cNewYorkRadioButton.gridx = 0; 
			cNewYorkRadioButton.gridy = 2;
			cNewYorkRadioButton.anchor 
				= GridBagConstraints.WEST;
			cNewYorkRadioButton.insets 
				= new Insets(4, 4, 4, 4);
			optionPanel.add(getNewYorkRadioButton(), 
							cNewYorkRadioButton);

			GridBagConstraints cAddButton 
				= new GridBagConstraints();
			cAddButton.gridx = 0; 
			cAddButton.gridy = 3;
			cAddButton.insets 
				= new Insets(4, 4, 4, 4);
			optionPanel.add(getAddButton(), 
							cAddButton);
		} catch (Throwable t) {
			handleException(t);
		}
	}
	return optionPanel;
}
private JRadioButton getParisRadioButton() {
	if (parisRadioButton == null) {
		try {
			parisRadioButton = new JRadioButton();
			parisRadioButton.setText("Paris");
		} catch (Throwable t) {
			handleException(t);
		}
	}
	return parisRadioButton;
}
private JLabel getTitleLabel() {
	if (titleLabel == null) {
		try {
			titleLabel = new JLabel();
			titleLabel.setText("Sale Of Coffee Beans For 2nd Quarter 2001 (In Millions)");
		} catch (Throwable t) {
			handleException(t);
		}
	}
	return titleLabel;
}
/**
 * Called whenever the part throws an exception.
 * @param exception java.lang.Throwable
 */
private void handleException(Throwable exception) {

	exception.printStackTrace(System.out);
}
private void initialize() {
	try {
		setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
		setSize(600, 480);
		setContentPane(getGraphPane());
		ButtonGroup b = new ButtonGroup();
		b.add(getLondonRadioButton());
		b.add(getParisRadioButton());
		b.add(getNewYorkRadioButton());

	} catch (Throwable t) {
		handleException(t);
	}
}
}
