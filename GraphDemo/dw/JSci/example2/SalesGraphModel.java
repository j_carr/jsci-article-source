package dw.JSci.example2;

/**
 * The model used by the BarGraph 
 * example
 * @author: John Carr
 */
import JSci.awt.*;
import java.util.*;
 
public class SalesGraphModel 
	   extends AbstractGraphModel 
			   implements CategoryGraph2DModel {
				   
   private String categoryNames[];
   private float seriesTotals[];

public SalesGraphModel(String categoryNames[],
					   float seriesTotals[]) {
   this.categoryNames 	= categoryNames;
   this.seriesTotals 	= seriesTotals;
}
public void firstSeries() { }
/**
 * Get the name of a category.
 */
public String getCategory(int i) {
   String name = categoryNames[i];
   float total = seriesTotals[i];

   StringBuffer sb = new StringBuffer(name);
   sb.append(" $");
   sb.append(total);
   return sb.toString();
}
/**
 * get the value of a category
 */
public float getValue(int i) {
   return seriesTotals[i]; 
}
/**
 * Add to a category total.
 */
public void incrementCategoryTotal(int i) {
   seriesTotals[i]++;	
}
public boolean nextSeries() {
   return false;
}
public int seriesLength() {
   return seriesTotals.length;
}
}
