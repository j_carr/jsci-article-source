The sample graphing programs are self-contained and should be able to run with Java 1.1.8, 1.2.2, 1.3.1 or higher

GraphDemo.jar is the self-contained executable jar with all library references. 
JSci.jar is the JSci package in the event you want to compile with the source. 

To execute from the command line:
	java -jar GraphDemo.jar 

To execute from Windows Explorer or on a Mac: 
	Double click on GraphDemo.jar


The source is provided as part of the article.
