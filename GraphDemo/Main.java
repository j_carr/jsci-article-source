/**
 * Main Program for bar, line, and 
 * pie graph examples
 * @author: John Carr
 */
import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import dw.JSci.example1.*;
import dw.JSci.example2.*; 
import dw.JSci.example3.*;

class Main extends JFrame 
			implements ActionListener {
				
	private JButton barGraphButton;
	private JButton lineGraphButton;
	private JPanel 	mainFrame;
	private JButton pieGraphButton;

public Main() {
    super("2D Line, Bar, and Pie Graphs" );
    initialize();
}
public void actionPerformed(ActionEvent e) {

try { 	
	if (e.getSource() == getPieGraphButton()) {
		PieGraph aPieGraph = new PieGraph();
		aPieGraph.setVisible(true);
	}

	else if (e.getSource() == getBarGraphButton()) {
		BarGraph aBarGraph = new BarGraph();
		aBarGraph.setVisible(true);
	}

	else if (e.getSource() == getLineGraphButton()) {
		LineGraph aLineGraph = new LineGraph();
		aLineGraph.setVisible(true);
	}	
}

catch (Throwable t) {
	handleException(t);
}

}
private JButton getBarGraphButton() {
	if (barGraphButton == null) {
		try {
			barGraphButton = new JButton();
			barGraphButton.setText("Bar Graph");
			barGraphButton.addActionListener(this);
		} catch (Throwable t) {
			handleException(t);
		}
	}
	return barGraphButton;
}
private JButton getLineGraphButton() {
	if (lineGraphButton == null) {
		try {
			lineGraphButton = new JButton();
			lineGraphButton.setText("Line Graph");
			lineGraphButton.addActionListener(this);
		} catch (Throwable t) {
			handleException(t);
		}
	}
	return lineGraphButton;
}
private JPanel getMainFrame() {
	if (mainFrame == null) {
		try {
			mainFrame = new JPanel();
			mainFrame.setLayout(new GridBagLayout());

			GridBagConstraints cPieGraphButton 
				= new GridBagConstraints();
			cPieGraphButton.gridx = 0; 
			cPieGraphButton.gridy = 0;
			cPieGraphButton.insets 
				= new Insets(4, 4, 4, 4);
			mainFrame.add(getPieGraphButton(), 
							cPieGraphButton);

			GridBagConstraints cBarGraphButton 
				= new GridBagConstraints();
			cBarGraphButton.gridx = 0; 
			cBarGraphButton.gridy = 1;
			cBarGraphButton.insets 
				= new Insets(4, 4, 4, 4);
			mainFrame.add(getBarGraphButton(), 
							cBarGraphButton);

			GridBagConstraints cLineGraphButton 
				= new GridBagConstraints();
			cLineGraphButton.gridx = 0; 
			cLineGraphButton.gridy = 2;
			cLineGraphButton.insets 
				= new Insets(4, 4, 4, 4);
			mainFrame.add(getLineGraphButton(), 
							cLineGraphButton);
		} catch (Throwable t) {
			handleException(t);
		}
	}
	return mainFrame;
}
private JButton getPieGraphButton() {
	if (pieGraphButton == null) {
		try {
			pieGraphButton = new JButton();
			pieGraphButton.setText("Pie Chart");
			pieGraphButton.addActionListener(this);
		} catch (Throwable t) {
			handleException(t);
		}
	}
	return pieGraphButton;
}
/**
 * Called whenever the part throws an exception.
 * @param exception java.lang.Throwable
 */
private void handleException(Throwable exception) {

	exception.printStackTrace(System.out);
}
private void initialize() {
	try {
		setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
		setSize(420, 240);
		setContentPane(getMainFrame());
	} catch (Throwable t) {
		handleException(t);
	}
}
/**
 * main entrypoint 
 */
public static void main(String[] args) {
	try {
        Main aMain;
        aMain = new Main();
        aMain.addWindowListener(new WindowAdapter() {
		public void windowClosing(WindowEvent e) {
			System.exit(0);
			};
		});
		aMain.setVisible(true);
	} catch (Throwable t) {
		System.err.println("Exception occurred");
		t.printStackTrace(System.out);
	}
}
}
